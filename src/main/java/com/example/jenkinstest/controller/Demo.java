package com.example.jenkinstest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/app")
public class Demo {
    @RequestMapping("/test")
    @ResponseBody
    public String testDemo() {
        return "success";
    }
}
